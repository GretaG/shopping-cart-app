<?php

use ShoppingCart\Files\TxtFileReader;
use ShoppingCart\Calculators\ShoppingCartCalculator;

require_once 'app/start.php';

$txtFileReader = new TxtFileReader();
$txtFileData = $txtFileReader->readTxtFile($argv[1]);

$cartCalculator = new ShoppingCartCalculator();
$products = $cartCalculator->getProducts($txtFileData);
$total = $cartCalculator->calculateProductsPrice($products);


echo "Total balance of the cart: " . $total;

