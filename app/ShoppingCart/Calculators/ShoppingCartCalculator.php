<?php

namespace ShoppingCart\Calculators;


class ShoppingCartCalculator
{
    private $currencies = array(
        "EUR" => 1,
        "USD" => 1.14,
        "GBP" => 0.88,
    );

    /**
     * Forms array from txt file data
     *
     * @param $givenData
     * @return array
     */
    function getProducts($givenData)
    {
        $productIdentifier = 0;
        $productQuantity = 2;
        $arrayLength = 5;
        $products = array();

        $givenData = explode("\r\n", $givenData);

        foreach ($givenData as $dataLine) {

            $dataLine = explode(";", $dataLine);

            if (count($dataLine) == $arrayLength) {
                if ($dataLine[$productQuantity] > 0) {
                    $products = $this->addProducts($products, $dataLine);
                } elseif ($dataLine[$productQuantity] < 0 && $products[$dataLine[$productIdentifier]]) {
                    $products = $this->deleteProducts($products, $dataLine);
                }
            }
        }

        return $products;
    }

    /**
     * Add product to products cart
     *
     * @param $products
     * @param $dataLine
     * @return mixed
     */
    function addProducts($products, $dataLine)
    {
        $productIdentifier = 0;
        $productName = 1;
        $productQuantity = 2;
        $productPrice = 3;
        $productCurrency = 4;

        $products[$dataLine[$productIdentifier]][] = array(
            "identifier" => $dataLine[$productIdentifier],
            "name" => $dataLine[$productName],
            "quantity" => $dataLine[$productQuantity],
            "price" => $dataLine[$productPrice],
            "currency" => $dataLine[$productCurrency],
        );

        Return $products;
    }

    /**
     * Delete product from products cart
     *
     * @param $products
     * @param $dataLine
     * @return mixed
     */
    function deleteProducts($products, $dataLine)
    {
        $productIdentifier = 0;
        $productQuantity = 2;

        $deletedFromCart = abs($dataLine[$productQuantity]);

        while ($deletedFromCart > 0) {

            $count = (count($products[$dataLine[$productIdentifier]]) - 1);
            $last = end($products[$dataLine[$productIdentifier]]);

            if ($last["quantity"] > $deletedFromCart) {
                $products[$dataLine[$productIdentifier]][$count]["quantity"] = $last["quantity"] - $deletedFromCart;
                $deletedFromCart = 0;
            } else {
                $deletedFromCart = $deletedFromCart - $last["quantity"];
                array_splice($products[$dataLine[$productIdentifier]], $count);
            }
        }

        Return $products;
    }

    /**
     * Calculate total cart price
     *
     * @param $products
     * @return int|string
     */
    function calculateProductsPrice($products)
    {
        $totalPrice = 0;
        foreach ($products as $product) {
            foreach ($product as $item) {
                $exchange_rate = $this->currencies[$item["currency"]];

                $itemPrice = number_format($exchange_rate * $item["price"] * $item["quantity"], 2);

                $totalPrice = $totalPrice + $itemPrice;
            }
        }

        return $totalPrice;
    }
}