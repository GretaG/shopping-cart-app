<?php

namespace ShoppingCart\Validators;

use \Exception;

class ValidateTxtReader extends Exception
{
    /**
     * @param $txtData
     */
    function validateData($txtData)
    {
        try {
            if (empty($txtData) || strlen($txtData) < 7) {
                throw new Exception('Missing data');
            }
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }
}