<?php
/**
 * Created by PhpStorm.
 * User: Kubas
 * Date: 2019-02-11
 * Time: 21:01
 */

namespace ShoppingCart\Files;

use ShoppingCart\Validators\ValidateTxtReader;

class TxtFileReader
{
    /**
     * Reads txt file
     *
     * @param $file
     * @return bool|string
     */
    function readTxtFile($file)
    {
        $txtFile = fopen($file, "r") or die("Unable to open file!");
        $txtFileData = fread($txtFile, filesize($file));
        fclose($txtFile);

        $validator = new ValidateTxtReader();
        $validator->validateData($txtFileData);

        return $txtFileData;
    }
}