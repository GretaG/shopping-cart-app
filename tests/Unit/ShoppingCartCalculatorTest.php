<?php
/**
 * Created by PhpStorm.
 * User: Kubas
 * Date: 2019-02-12
 * Time: 16:59
 */

use PHPUnit\Framework\TestCase;

class ShoppingCartCalculatorTest extends TestCase
{
    protected $products = array(
        "mbp" => array(
            0 => array(
                "identifier" => "mbp",
                "name" => "Macbook Pro",
                "quantity" => 2,
                "price" => 29.99,
                "currency" => "EUR"

            )
        ),
        "zen" => array(
            0 => array(
                "identifier" => "zen",
                "name" => "Asus Zenbook",
                "quantity" => 2,
                "price" => 88.99,
                "currency" => "USD"

            ),
        ),
    );

    protected $calculator;

    public function setUp()
    {
        $this->calculator = new ShoppingCart\Calculators\ShoppingCartCalculator;
    }

    public function testGetProducts()
    {
        $txtData = "mbp;Macbook Pro;2;29.99;EUR
zen;Asus Zenbook;2;88.99;USD";

        $result = $this->calculator->getProducts($txtData);

        $this->assertEquals($result, $this->products);
    }

    public function testAddProductToProductsArray()
    {
        $returnedProducts = array(
            "mbp" => array(
                0 => array(
                    "identifier" => "mbp",
                    "name" => "Macbook Pro",
                    "quantity" => 2,
                    "price" => 29.99,
                    "currency" => "EUR"

                )
            ),
            "zen" => array(
                0 => array(
                    "identifier" => "zen",
                    "name" => "Asus Zenbook",
                    "quantity" => 2,
                    "price" => 88.99,
                    "currency" => "USD"

                ),
                1 => array(
                    "identifier" => "zen",
                    "name" => "Asus Zenbook",
                    "quantity" => 3,
                    "price" => 99.99,
                    "currency" => "USD"

                ),
            ),
        );

        $dataLine = array(
            "0" => "zen",
            "1" => "Asus Zenbook",
            "2" => 3,
            "3" => 99.99,
            "4" => "USD"
        );

        $result = $this->calculator->addProducts($this->products, $dataLine);

        $this->assertEquals($result, $returnedProducts);
    }

    public function testDeleteProductFromProductsArray()
    {
        $returnedProducts = array(
            "mbp" => array(
                0 => array(
                    "identifier" => "mbp",
                    "name" => "Macbook Pro",
                    "quantity" => 2,
                    "price" => 29.99,
                    "currency" => "EUR"

                )
            ),
            "zen" => array(
                0 => array(
                    "identifier" => "zen",
                    "name" => "Asus Zenbook",
                    "quantity" => 1,
                    "price" => 88.99,
                    "currency" => "USD"

                ),
            ),
        );

        $dataLine = array(
            "0" => "zen",
            "1" => "Asus Zenbook",
            "2" => -1,
            "3" => "",
            "4" => ""
        );

        $result = $this->calculator->deleteProducts($this->products, $dataLine);

        $this->assertEquals($result, $returnedProducts);
    }

    public function testCalculateTotalProductsPrice()
    {
        $result = $this->calculator->calculateProductsPrice($this->products);

        $this->assertEquals($result,"262.88");
    }


}