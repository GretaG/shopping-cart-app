<?php
/**
 * Created by PhpStorm.
 * User: Kubas
 * Date: 2019-02-12
 * Time: 13:38
 */

use PHPUnit\Framework\TestCase;

class TxtFileReaderTest extends TestCase
{
    public $txtData = "mbp;Macbook Pro;2;29.99;EUR
zen;Asus Zenbook;2;88.99;USD
zen;Asus Zenbook;3;99.99;USD";

    public function testReadingWhenTheFileIsValid()
    {
        $txtReader = new ShoppingCart\Files\TxtFileReader();

        $result = $txtReader->readTxtFile("tests\Helpers\shopping-cart.txt");

        $this->assertEquals($result, $this->txtData);
    }
}