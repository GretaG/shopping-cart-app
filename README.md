# shopping-cart-app

## Goal

- Best value with least effort (in the shortest time)
- No unnecessary functions that are not requested by task
- Tests have been added using good practice

## Technical Requirements

- PHP7 + / 7.1 was used
- Recommended to use `Composer` for autoloading / autoload psr-4 was used
- 3rd party libraries can be used if needed / not needed
- App should be executed from console / executed with command "php script.php shopping-cart.txt"
- Solution's source code should be provided in a GIT repository / provided in a GIT repository

### Setup

Run commands

```
> composer install
```

```
> composer dump-autoload
```

### Executed with command

```
> php script.php shopping-cart.txt
```

### Run tests

```
> vendor/bin/phpunit
```